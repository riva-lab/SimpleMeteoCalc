
# SimpleMeteoCalc

Библиотека Arduino для расчета некоторых метеопараметров из базовых T-P-H, полученных от метеодатчиков. Также поддерживается конвертирование в другие единицы измерения.



## Возможности

##### Библиотека предоставляет класс для расчета следующих величин:

- температура в Кельвинах,
- температура в градусах Фаренгейта,
- эффективная температура в градусах Цельсия,
- абсолютная влажность в граммах на кубометр,
- точка росы в градусах Цельсия,
- давление насыщенного водяного пара в паскалях,
- парциальное давление водяного пара в паскалях,
- давление в миллиметрах ртутного столба,
- высота над уровнем моря от давления, метры,
- нормальное давление на высоте установки барометра в паскалях,
- нормальное давление на высоте установки барометра в миллиметрах ртутного столба.

##### Входные данные:

- температура в градусах Цельсия,
- влажность относительная в %,
- давление в паскалях,
- высота барометра над уровнем моря в метрах.



## Установка

### Используя Arduino IDE Library Manager

1. Откройте Менеджер библиотек `Скетч > Подключить библиотеку > Управлять библиотеками...` (`Ctrl+Shift+I`).
2. Введите `SimpleMeteoCalc` в поле поиска.
3. Выберите строку с библиотекой.
4. Выберите версию и щелкните кнопку `Установка`.

### Используя Git

```sh
cd ~/Documents/Arduino/libraries/
git clone https://gitlab.com/riva-lab/SimpleMeteoCalc SimpleMeteoCalc
```



## Программный интерфейс

### Установка исходных данных

```c++
void setTemperature(float celsius); // температура в градусах Цельсия
void setHumidity(uint8_t percents); // влажность относительная в %
void setPressure(float pascals);    // давление в паскалях
void setUserAltitude(float meters); // высота барометра над уровнем моря в метрах
```

*Примечание*. Высота барометра — это реальная высота над уровнем моря того места, в котором будет работать барометр. Эту величину можно узнать с помощью карты высот, найдя высоту точки с географическими координатами места установки.

### Получение исходных данных

```c++
float getTemperature();             // температура в градусах Цельсия
float getHumidity();                // влажность относительная в %
float getPressure();                // давление в паскалях
float getUserAltitude();            // высота барометра над уровнем моря в метрах
```

### Расчет значений

```c++
void calculate();
```

### Получение рассчитанных значений

```c++
float getDewPoint();                // точка росы в градусах Цельсия (t,h)
float getSteamPressureSaturated();  // давление насыщенного водяного пара в паскалях (t,p)
float getSteamPressurePartial();    // парциальное давление водяного пара в паскалях (t,p,h)
float getHumidityAbsolute();        // абсолютная влажность в граммах на кубометр (t,p,h)
float getTemperatureEffective();    // эффективная температура в градусах Цельсия (t,p,h)
float getKelvins();                 // температура в Кельвинах (t)
float getFahrenheits();             // температура в градусах Фаренгейта (t)
float getPressuremmHg();            // давление в миллиметрах ртутного столба (p)
float getAltitude();                // высота над уровнем моря от давления, метры (t,p)
float getNormalPressure();          // нормальное давление на высоте установки барометра в паскалях (t,a)
float getNormalPressuremmHg();      // нормальное давление на высоте установки барометра в мм рт. ст. (t,a)
```

*Примечание*. В скобках после описания указаны исходные данные, от которых зависит рассчитываемая величина: **t** — температура, **p** — давление, **h** — влажность, **a** — высота.

### Отдельные функции-конвертеры (не входят в класс SimpleMeteoCalc)

```c++
float celsiusToKelvins(float celcius);
float celsiusToFahrenheits(float celcius);
float pascalsTommHg(float pascals);
float pascalsToAltitude(float pascals, float celsius = 20);
```



## Примеры

##### Простейший пример

```c++
#include "SimpleMeteoCalc.h"
SimpleMeteoCalc mc;

void setup() {
  Serial.begin(115200);

  // Здесь в качестве исходных данных использованы константы,
  // в реальном проекте тут передаются данные с датчиков
  mc.setTemperature(18.0);
  mc.setHumidity(40.0);
  mc.setPressure(98500.0);
  mc.setUserAltitude(500.0);

  // Расчет
  mc.calculate();

  // Контрольное считывание исходных данных
  float t = mc.getTemperature();
  float h = mc.getHumidity();
  float p = mc.getPressure();
  float a = mc.getUserAltitude();

  // Считывание расчитанных значений
  float td = mc.getDewPoint();
  float ps = mc.getSteamPressureSaturated();
  float pp = mc.getSteamPressurePartial();
  float ha = mc.getHumidityAbsolute();
  float te = mc.getTemperatureEffective();
  float tk = mc.getKelvins();
  float tf = mc.getFahrenheits();
  float mm = mc.getPressuremmHg();
  float hp = mc.getAltitude();
  float ap = mc.getNormalPressure();
  float am = mc.getNormalPressuremmHg();

  // Вывод в терминал
  Serial.print(F("INPUT:\t\tt="));   Serial.print(t, 3);
  Serial.print(F(" *C\th="));        Serial.print(h, 3);
  Serial.print(F(" %\tp="));         Serial.print(p, 3);
  Serial.print(F(" Pa\ta="));        Serial.print(a, 3);
  Serial.print(F(" m\nAp="));        Serial.print(ap, 3);
  Serial.print(F(" Pa\tAm="));       Serial.print(am, 3);
  Serial.print(F(" mmHg\nTd="));     Serial.print(td, 3);
  Serial.print(F(" *C\tTe="));       Serial.print(te, 3);
  Serial.print(F(" *C\tTK="));       Serial.print(tk, 3);
  Serial.print(F(" K\tTF="));        Serial.print(tf, 3);
  Serial.print(F(" *F\nPs="));       Serial.print(ps, 3);
  Serial.print(F(" Pa\tPp="));       Serial.print(pp, 3);
  Serial.print(F(" Pa\tmm="));       Serial.print(mm, 3);
  Serial.print(F(" mmHg\tPA="));     Serial.print(hp, 3);
  Serial.print(F(" m\nHa="));        Serial.print(ha, 3);
  Serial.print(F(" g/m^3\n\n"));
}

void loop() {
}
```

Подробные примеры смотрите в каталоге [examples](examples).



## Лицензия

Библиотека распространяется под [лицензией](license) [MIT](https://opensource.org/licenses/mit-license.php).



## Получение исходных метеоданных

Для получения исходных метеоданных можно использовать единственный датчик [BME280](https://gitlab.com/riva-lab/SimpleBME280). Можно также использовать другие датчики, в том числе отдельные, например, DS18B20, LM35DZ, BMP180, BMP280, BMP388, BMP388, BME680, MS5611, AM2320, DHT11, DHT21, DHT22, SHT20, SHT30, SHT31, HTU21, HR31 и множество других. Обратите внимание, для работы с используемыми датчиками требуются соответствующие библиотеки, которые позволят считать данные из этих датчиков.

