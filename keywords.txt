#######################################
# Syntax Coloring Map For SimpleMeteoCalc
#######################################

#######################################
# Datatypes (KEYWORD1)
# editor.data_type.style
#######################################

SimpleMeteoCalc				KEYWORD1

#######################################
# Methods and Functions (KEYWORD2)
# editor.function.style
#######################################

setTemperature				KEYWORD2
setHumidity					KEYWORD2
setPressure					KEYWORD2
setUserAltitude				KEYWORD2

getTemperature				KEYWORD2
getHumidity					KEYWORD2
getPressure					KEYWORD2
getUserAltitude				KEYWORD2

calculate					KEYWORD2
getDewPoint					KEYWORD2
getSteamPressureSaturated	KEYWORD2
getSteamPressurePartial		KEYWORD2
getHumidityAbsolute			KEYWORD2
getTemperatureEffective		KEYWORD2
getKelvins					KEYWORD2
getFahrenheits				KEYWORD2
getPressuremmHg				KEYWORD2
getAltitude					KEYWORD2
getNormalPressure			KEYWORD2
getNormalPressuremmHg		KEYWORD2

celsiusToKelvins			KEYWORD2
celsiusToFahrenheits		KEYWORD2
pascalsTommHg				KEYWORD2
pascalsToAltitude			KEYWORD2

#######################################
# Structures (KEYWORD3)
# editor.function.style
#######################################

#######################################
# Constants (LITERAL1)
# editor.reserved_word_2.style
#######################################

SIMPLEMETEOCALC_VERSION		LITERAL1
