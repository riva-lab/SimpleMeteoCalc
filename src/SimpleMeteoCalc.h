#ifndef SIMPLEMETEOCALC_H
#define SIMPLEMETEOCALC_H

#include <stdint.h>

#define SIMPLEMETEOCALC_VERSION         0x10


// отдельные функции-конвертеры
float celsiusToKelvins(float celcius);
float celsiusToFahrenheits(float celcius);
float pascalsTommHg(float pascals);
float pascalsToAltitude(float pascals, float celsius = 20);


// работа с метеопараметрамы t,h,p (температура, влажность, давление)
class SimpleMeteoCalc {

  public:

    // установка исходных данных
    void setTemperature(float celsius); // температура в градусах Цельсия
    void setHumidity(uint8_t percents); // влажность относительная в %
    void setPressure(float pascals);    // давление в паскалях
    void setUserAltitude(float meters); // высота барометра над уровнем моря в метрах

    // получение исходных данных
    float getTemperature();             // температура в градусах Цельсия
    float getHumidity();                // влажность относительная в %
    float getPressure();                // давление в паскалях
    float getUserAltitude();            // высота барометра над уровнем моря в метрах

    // расчет значений
    void calculate();

    // получение расчитанных значений; после описания указаны исходные данные, от которых зависит рассчитываемая величина
    float getDewPoint();                // точка росы в градусах Цельсия (t,h)
    float getSteamPressureSaturated();  // давление насыщенного водяного пара в паскалях (t,p)
    float getSteamPressurePartial();    // парциальное давление водяного пара в паскалях (t,p,h)
    float getHumidityAbsolute();        // абсолютная влажность в граммах на кубометр (t,p,h)
    float getTemperatureEffective();    // эффективная температура в градусах Цельсия (t,p,h)
    float getKelvins();                 // температура в Кельвинах (t)
    float getFahrenheits();             // температура в градусах Фаренгейта (t)
    float getPressuremmHg();            // давление в миллиметрах ртутного столба (p)
    float getAltitude();                // высота над уровнем моря от давления, метры (t,p)
    float getNormalPressure();          // нормальное давление на высоте установки барометра в паскалях (t,a)
    float getNormalPressuremmHg();      // нормальное давление на высоте установки барометра в мм рт. ст. (t,a)

  private:

    float t;
    float h;
    float p;
    float a;
    float td;
    float te;
    float ps;
    float pp;
    float pn;
    float ha;
    float hp;
};

#endif /* SIMPLEMETEOCALC_H */
