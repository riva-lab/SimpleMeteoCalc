#include "SimpleMeteoCalc.h"

SimpleMeteoCalc mc;

void setup() {
  Serial.begin(115200);

  // Здесь в качестве исходных данных использованы константы, в реальном проекте тут передаю данные с датчиков
  mc.setTemperature(18.0);
  mc.setHumidity(40.0);
  mc.setPressure(98500.0);
  mc.setUserAltitude(500.0);
}

void loop() {
  uint32_t t1 = micros();
  mc.calculate();
  t1 = micros() - t1;

  uint32_t t2 = micros();
  float t = mc.getTemperature();
  float h = mc.getHumidity();
  float p = mc.getPressure();
  float a = mc.getUserAltitude();

  float td = mc.getDewPoint();
  float ps = mc.getSteamPressureSaturated();
  float pp = mc.getSteamPressurePartial();
  float ha = mc.getHumidityAbsolute();
  float te = mc.getTemperatureEffective();
  float tk = mc.getKelvins();
  float tf = mc.getFahrenheits();
  float mm = mc.getPressuremmHg();
  float hp = mc.getAltitude();
  float ap = mc.getNormalPressure();
  float am = mc.getNormalPressuremmHg();
  t2 = micros() - t2;

  Serial.print(F("INPUT:\t\tt="));   Serial.print(t, 3);
  Serial.print(F(" *C\th="));        Serial.print(h, 3);
  Serial.print(F(" %\tp="));         Serial.print(p, 3);
  Serial.print(F(" Pa\ta="));        Serial.print(a, 3);

  Serial.print(F(" m\nAp="));        Serial.print(ap, 3);
  Serial.print(F(" Pa\tAm="));       Serial.print(am, 3);

  Serial.print(F(" mmHg\nTd="));     Serial.print(td, 3);
  Serial.print(F(" *C\tTe="));       Serial.print(te, 3);
  Serial.print(F(" *C\tTK="));       Serial.print(tk, 3);
  Serial.print(F(" K\tTF="));        Serial.print(tf, 3);

  Serial.print(F(" *F\nPs="));       Serial.print(ps, 3);
  Serial.print(F(" Pa\tPp="));       Serial.print(pp, 3);
  Serial.print(F(" Pa\tmm="));       Serial.print(mm, 3);
  Serial.print(F(" mmHg\tPA="));     Serial.print(hp, 3);

  Serial.print(F(" m\nHa="));                   Serial.print(ha, 3);
  Serial.print(F(" g/m^3\tTIMINGS:\tcalc="));   Serial.print(t1);
  Serial.print(F(" us\tout="));                 Serial.print(t2);
  Serial.print(F(" us\n\n"));

  delay(1000);
}
