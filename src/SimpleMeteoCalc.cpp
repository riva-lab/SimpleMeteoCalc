#include "SimpleMeteoCalc.h"
#include <Math.h>

// коэффициенты для расчетов
#define DEW_A           17.27
#define DEW_B           237.7
#define PSAT_FA         1.0016
#define PSAT_FB         3.15e-8
#define PSAT_FC         7.4
#define PSAT_EA         17.62
#define PSAT_EB         243.12
#define PSAT_EM         611.2
#define PRESS_NORMAL    101325.0
#define PRESS_P2MMHG(p) (p * (760.0 / PRESS_NORMAL))
#define ALT_COEFF       29.256
#define HUM_RV          461.5e-3
#define TEMP_C2K(c)     (273.15 + c)
#define TEMP_C2F(c)     (c * (9.0 / 5.0) + 32)
#define TEFF_D          4.25
#define TEFF_M          3.78e-3

void SimpleMeteoCalc::setTemperature(float celsius) {
  t = celsius;
}

void SimpleMeteoCalc::setHumidity(uint8_t percents) {
  h = (float)percents / 100.0;
}

void SimpleMeteoCalc::setPressure(float pascals) {
  p = pascals;
}

void SimpleMeteoCalc::setUserAltitude(float meters) {
  a = meters;
}

float SimpleMeteoCalc::getTemperature() {
  return t;
}

float SimpleMeteoCalc::getHumidity() {
  return 100.0 * h;
}

float SimpleMeteoCalc::getPressure() {
  return p;
}

float SimpleMeteoCalc::getUserAltitude() {
  return a;
}

void SimpleMeteoCalc::calculate() {
  float tmp;

  /* Точка росы, в гр.Цельсия
           b f(T,RH)              a T
     Tp = -----------; f(T,RH) = ----- + ln RH; a = 17,27; b = 237,7; T = [гр.C]; 0 < RH < 1
          a - f(T,RH)            b + T
  */
  tmp  = DEW_A * t;
  tmp /= DEW_B + t;
  tmp += log(h);
  td   = DEW_B * tmp;
  td  /= DEW_A - tmp;

  /* Давление насыщенного водяного пара
                                              17,62 T                           -8    7,4
     P(H2Oнас) = e(t) f(p); e(t) = 611,2 exp ----------; f(p) = 1,0016 + 3,15*10  P - ---; P = [Па]; T = [гр.C]
                                             243,12 + T                                P
  */
  tmp  = PSAT_EA * t;
  tmp /= PSAT_EB + t;
  tmp  = exp(tmp);
  tmp *= PSAT_EM;
  ps   = PSAT_FA;
  ps  += PSAT_FB * p;
  ps  -= PSAT_FC / p;
  ps  *= tmp;

  /* Парциальное давление водяного пара
     P(H2O) = P(H2Oнас) RH; 0 < RH < 1
  */
  pp  = ps;
  pp *= h;

  /* Абсолютная влажность в г/м^3
              P(H2O)                     -3
     AH = ----------------; Rv = 461,5*10  ; P = [Па]; T = [гр.C]
          Rv (273,15 + t)
  */
  ha  = pp;
  ha /= HUM_RV;
  ha /= TEMP_C2K(t);

  /* Эффективная температура (как ощущается)
                      -3
     Tэф = t + 3,78*10  e(t) RH - 4,25; Tэф, t = [гр.C]; e(t) = [Па]; 0 < RH < 1
  */
  te  = pp;
  te *= TEFF_M;
  te -= TEFF_D;
  te += t;

  /* Высота над уровнем моря от давления, метры
                 Po
     h = C T ln ----; C = 29,256; T = [K] = 273,15 + t[гр.C]
                 Px
  */
  hp  = PRESS_NORMAL;
  hp /= p;
  hp  = log(hp);
  hp *= TEMP_C2K(t);
  hp *= ALT_COEFF;

  // Нормальное давление
  pn  = a;
  pn /= ALT_COEFF;
  pn /= TEMP_C2K(t);
  pn  = exp(pn);
  pn  = PRESS_NORMAL / pn;
}

float SimpleMeteoCalc::getDewPoint() {
  return td;
}

float SimpleMeteoCalc::getSteamPressureSaturated() {
  return ps;
}

float SimpleMeteoCalc::getSteamPressurePartial() {
  return pp;
}

float SimpleMeteoCalc::getHumidityAbsolute() {
  return ha;
}

float SimpleMeteoCalc::getTemperatureEffective() {
  return te;
}

float SimpleMeteoCalc::getKelvins() {
  return TEMP_C2K(t);
}

float SimpleMeteoCalc::getFahrenheits() {
  return TEMP_C2F(t);
}

float SimpleMeteoCalc::getPressuremmHg() {
  return PRESS_P2MMHG(p);
}

float SimpleMeteoCalc::getAltitude() {
  return hp;
}

float SimpleMeteoCalc::getNormalPressure() {
  return pn;
}

float SimpleMeteoCalc::getNormalPressuremmHg() {
  return PRESS_P2MMHG(pn);
}


// *** отдельные функции-конвертеры

float celsiusToKelvins(float celcius) {
  return TEMP_C2K(celcius);
}

float celsiusToFahrenheits(float celcius) {
  return TEMP_C2F(celcius);
}

float pascalsTommHg(float pascals) {
  return PRESS_P2MMHG(pascals);
}

float pascalsToAltitude(float pascals, float celsius) {
  float hp = PRESS_NORMAL;
  hp /= pascals;
  hp  = log(hp);
  hp *= TEMP_C2K(celsius);
  hp *= ALT_COEFF;
  return hp;
}
